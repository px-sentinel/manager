package net.poundex.sentinel.ui.manager.hue.ui

import javafx.beans.property.ReadOnlyStringWrapper
import javafx.collections.ObservableList
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.BorderPane

class HueWindowView extends TabPane
{
	private final HueWindow hueWindow

	private final ObservableList<Map> bridges
	private final ObservableList<Map> bulbs

	private TableView<Map> bridgesView
	private TableView<Map> bulbsView

	HueWindowView(HueWindow hueWindow, ObservableList<Map> bridges, ObservableList<Map> bulbs)
	{
		this.hueWindow = hueWindow
		this.bridges = bridges
		this.bulbs = bulbs

		setTabClosingPolicy TabClosingPolicy.UNAVAILABLE
		tabs << new Tab("Bridges", createBridgeView())
		tabs << new Tab("Bulbs", createBulbView())
	}

	Node createBridgeView()
	{
		BorderPane bp = new BorderPane()
		Button add = new Button("Add")
		add.setOnAction hueWindow.&onAddClicked

		Button edit = new Button("Edit")
		edit.setOnAction hueWindow.&onEditClicked

		Button delete = new Button("Delete")
		delete.setOnAction hueWindow.&onDeleteClicked

		bp.setTop(new ToolBar(add, edit, delete))
		bridgesView = createBridgesView()
		bp.setCenter(bridgesView)
		return bp
	}

	private TableView createBridgesView() {
		TableColumn<Map, Map> colName = new TableColumn<>("Name")
		TableColumn<Map, Map> colAddress = new TableColumn<>("Address")
		TableView<Map> table = new TableView<>(bridges)
		table.columns.addAll colName, colAddress
		colName.setCellValueFactory { f ->
			new ReadOnlyStringWrapper(f.value.name)
		}
		colAddress.setCellValueFactory { f ->
			new ReadOnlyStringWrapper(f.value.address)
		}

		return table
	}

	Node createBulbView()
	{
		TableColumn<Map, Map> colName = new TableColumn<>("Name")
		TableColumn<Map, Map> colBridge = new TableColumn<>("Bridge")
		TableColumn<Map, Map> colType = new TableColumn<>("Type")
		TableColumn<Map, Map> colManufacturer = new TableColumn<>("Manufacturer")
		TableColumn<Map, Map> colModel = new TableColumn<>("Model ID")
		TableColumn<Map, Map> colProductName = new TableColumn<>("Product Name")
		TableColumn<Map, Map> colUniqueId = new TableColumn<>("Unique ID")

		TableView<Map> table = new TableView<>(bulbs)
		table.columns.addAll colName, colBridge, colType, colManufacturer, colModel, colProductName, colUniqueId
		colName.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.name) }
		colBridge.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.hardware.name) }
		colType.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.type) }
		colManufacturer.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.manufacturer) }
		colModel.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.modelId) }
		colProductName.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.productName) }
		colUniqueId.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.uniqueId) }

		return table
	}

	Map getCurrent()
	{
		bridgesView.focusModel.focusedItem
	}
}

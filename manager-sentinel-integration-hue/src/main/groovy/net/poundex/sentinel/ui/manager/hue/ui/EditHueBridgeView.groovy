package net.poundex.sentinel.ui.manager.hue.ui

import javafx.scene.control.Button
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormView

class EditHueBridgeView extends ResourceFormView<EditHueBridgeForm>
{
	EditHueBridgeView(EditHueBridgeWindow editHueBridgeWindow)
	{
		super(editHueBridgeWindow)
	}

	@Override
	protected List<List<Button>> getAdditionalButtons()
	{
		return [[new Button("Acquire Username")]]

	}
}

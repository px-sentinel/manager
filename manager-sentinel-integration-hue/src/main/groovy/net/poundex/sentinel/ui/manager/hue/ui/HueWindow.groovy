package net.poundex.sentinel.ui.manager.hue.ui

import groovy.transform.PackageScope
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class HueWindow implements Window
{
	final static String ROLE = "HueWindow"
	final String title = "Hue"

	final HueWindowView view

	private final WindowManager windowManager
	private final ObjectFactory<EditHueBridgeWindow> editHueBridgeWindowObjectFactory
	private final ResourceClient resourceClient

	private final ObservableList<Map> bridges = FXCollections.observableArrayList()
	private final ObservableList<Map> bulbs = FXCollections.observableArrayList()

	HueWindow(WindowManager windowManager, ObjectFactory<EditHueBridgeWindow> editHueBridgeWindowObjectFactory, ResourceClient resourceClient) {
		this.windowManager = windowManager
		this.editHueBridgeWindowObjectFactory = editHueBridgeWindowObjectFactory
		this.resourceClient = resourceClient
		this.view = new HueWindowView(this, bridges, bulbs)
		refresh()
	}

	private void refresh() {
		bridges.clear()
		bridges.addAll(resourceClient.findAll("hue/bridge"))

		bulbs.clear()
		bulbs.addAll(resourceClient.findAll("hue/device"))
	}

	@PackageScope
	void onAddClicked(ActionEvent ae) {
		windowManager.activateWindow(EditHueBridgeWindow.role("new")) {
			EditHueBridgeWindow w = editHueBridgeWindowObjectFactory.getObject()
			w.parentWindowNotifier = this.&refresh
			EditHueBridgeForm f = new EditHueBridgeForm()
			f.name = "hue" + (bridges.size() > 0 ? bridges.size() : 0)
			w.form = f
			return w
		}
	}

	@PackageScope
	void onEditClicked(ActionEvent ae) {
		Map current = view.current
		if( ! current) return
		windowManager.activateWindow(EditHueBridgeWindow.role("${current.id}")) {
			EditHueBridgeWindow w = editHueBridgeWindowObjectFactory.object
			w.parentWindowNotifier = this.&refresh
			EditHueBridgeForm form = new EditHueBridgeForm()
			form.id = current.id
			form.applyFromMap(current)
			w.form = form
			return w
		}
	}

	@PackageScope
	void onDeleteClicked(ActionEvent ae) {
		Map current = view.current
		if( ! current) return
		resourceClient.delete("hueBridge", current.id)
		refresh()
	}
}

package net.poundex.sentinel.ui.manager.hue.ui


import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Lazy
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
class EditHueBridgeWindow extends ResourceFormWindow<EditHueBridgeForm>
{
	final String title = "Edit Hue Bridge"

	EditHueBridgeWindow(ResourceClient resourceClient)
	{
		super(resourceClient, "hue/bridge", new EditHueBridgeView(this))
	}

	static String role(String s)
	{
		return "EditHueBridgeWindow/" + s
	}

}

package net.poundex.sentinel.ui.manager.hue.ui

import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Component
class HueMenuItem implements MenuItem
{
	final String label = "Hue"

	private final WindowManager windowManager
	private final ObjectFactory<HueWindow> hueWindowObjectFactory

	HueMenuItem(WindowManager windowManager, ObjectFactory<HueWindow> hueWindowObjectFactory)
	{
		this.windowManager = windowManager
		this.hueWindowObjectFactory = hueWindowObjectFactory
	}

	@Override
	void activate()
	{
		windowManager.activateWindow(HueWindow.ROLE, hueWindowObjectFactory.&getObject)
	}
}

package net.poundex.sentinel.ui.manager.hue.ui

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import net.poundex.sentinel.ui.manager.FormBean

import javax.validation.constraints.NotBlank

class EditHueBridgeForm extends FormBean
{
	StringProperty name = new SimpleStringProperty()
	StringProperty address = new SimpleStringProperty()
	StringProperty username = new SimpleStringProperty()

	@NotBlank
	String getName() {
		return name.value
	}

	@NotBlank
	String getAddress() {
		return address.value
	}

	@NotBlank
	String getUsername() {
		return username.value
	}

	void setName(String name)
	{
		this.name.setValue(name)
	}

	void setAddress(String address)
	{
		this.address.value = address
	}

	void setUsername(String username)
	{
		this.username.value = username
	}

	@Override
	Map<String, ?> toMap()
	{
		return [name: getName(), address: getAddress(), username: getUsername()]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		setName(map.name)
		setAddress(map.address)
		setUsername(map.username)
	}
}

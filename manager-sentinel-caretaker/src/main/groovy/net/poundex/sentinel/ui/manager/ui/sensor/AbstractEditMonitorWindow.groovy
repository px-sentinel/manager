package net.poundex.sentinel.ui.manager.ui.sensor

import com.dooapp.fxform.FXForm
import com.dooapp.fxform.filter.ReorderFilter
import com.dooapp.fxform.view.factory.DefaultFactoryProvider
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.ui.EntityChooser
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow

abstract class AbstractEditMonitorWindow<F extends AbstractEditMonitorForm> extends ResourceFormWindow<F> implements EntityChooser
{
	final String title = "Edit Monitor"

	AbstractEditMonitorWindow(ResourceClient resourceClient, String resourceName)
	{
		super(resourceClient, resourceName)
	}

	abstract List<String> getFormFields()

	@Override
	protected void customiseFactoryProvider(DefaultFactoryProvider defaultFactoryProvider)
	{
		entityChooser(defaultFactoryProvider, "location", "zone")
	}

	@Override
	protected void doWithForm(FXForm form)
	{
		form.addFilters new ReorderFilter(formFields as String[])
	}


}

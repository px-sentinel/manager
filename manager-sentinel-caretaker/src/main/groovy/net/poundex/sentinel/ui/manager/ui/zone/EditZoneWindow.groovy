package net.poundex.sentinel.ui.manager.ui.zone


import com.dooapp.fxform.view.factory.DefaultFactoryProvider
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.ui.EntityChooser
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class EditZoneWindow extends ResourceFormWindow<EditZoneForm> implements EntityChooser
{
	final String title = "Edit Zone"

	EditZoneWindow(ResourceClient resourceClient)
	{
		super(resourceClient, "zone")
	}

	static String role(String s)
	{
		return "EditZoneWindow/${s}"
	}

	@Override
	protected void customiseFactoryProvider(DefaultFactoryProvider defaultFactoryProvider)
	{
		entityChooser(defaultFactoryProvider, "parent", "zone")
	}
}

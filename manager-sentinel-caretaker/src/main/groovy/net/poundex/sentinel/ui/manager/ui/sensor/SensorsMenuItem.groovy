package net.poundex.sentinel.ui.manager.ui.sensor

import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Component
class SensorsMenuItem implements MenuItem
{
	final String label = "Sensors"

	private final WindowManager windowManager
	private final ObjectFactory<SensorsWindow> sensorsWindowObjectFactory

	SensorsMenuItem(WindowManager windowManager, ObjectFactory<SensorsWindow> sensorsWindowObjectFactory)
	{
		this.windowManager = windowManager
		this.sensorsWindowObjectFactory = sensorsWindowObjectFactory
	}

	@Override
	void activate()
	{
		windowManager.activateWindow(SensorsWindow.ROLE, sensorsWindowObjectFactory.&getObject)
	}
}

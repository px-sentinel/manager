package net.poundex.sentinel.ui.manager.ui.light

import javafx.beans.property.ReadOnlyStringWrapper
import javafx.collections.ObservableList
import javafx.scene.control.Button
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.ToolBar
import javafx.scene.layout.BorderPane

class LightsView extends BorderPane
{
	private final LightsWindow lightsWindow

	private final ObservableList<Map> lights

	private TableView<Map> lightsView

	LightsView(LightsWindow lightsWindow, ObservableList<Map> lights)
	{
		this.lightsWindow = lightsWindow
		this.lights = lights
		init()
	}

	void init()
	{
		setTop createToolbar()
		lightsView = createLightsView()
		setCenter lightsView
	}

	private ToolBar createToolbar()
	{
		Button add = new Button("Add")
		add.setOnAction lightsWindow.&onAddClicked

		Button edit = new Button("Edit")
		edit.setOnAction lightsWindow.&onEditClicked

		Button delete = new Button("Delete")
		delete.setOnAction lightsWindow.&onDeleteClicked

		return new ToolBar(add, edit, delete)
	}

	private TableView<Map> createLightsView()
	{
		TableColumn<Map, Map> colName = new TableColumn<>("Name")
		TableView<Map> table = new TableView<>(lights)
		colName.setCellValueFactory { f -> new ReadOnlyStringWrapper(f.value.name) }
		table.columns << colName
		return table
	}

	Map getCurrent()
	{
		return lightsView.focusModel.getFocusedItem()
	}
}

package net.poundex.sentinel.ui.manager.ui.sensor


import javafx.beans.property.SimpleStringProperty
import net.poundex.sentinel.ui.manager.FormBean

import javax.validation.constraints.NotBlank

class EditValueReaderForm extends FormBean
{
	SimpleStringProperty name = new SimpleStringProperty()
	SimpleStringProperty valueSource = new SimpleStringProperty()
	SimpleStringProperty transformer = new SimpleStringProperty()
//	ListProperty sources = new SimpleListProperty()

	@NotBlank
	String getName()
	{
		return name.value
	}

	void setName(String name)
	{
		this.name.setValue(name)
	}

	@NotBlank
	String getValueSource()
	{
		return valueSource.value
	}

	void setValueSource(String valueSource)
	{
		this.valueSource.setValue(valueSource)
	}

	String getTransformer()
	{
		return transformer.value
	}

	void setTransformer(String transformer)
	{
		this.transformer.setValue(transformer)
	}

	@Override
	Map<String, ?> toMap()
	{
		return [name: getName(), valueSource: getValueSource(), transformer: getTransformer()]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		setName(map.name)
		setValueSource(map.valueSource)
		setTransformer(map.transformer)
	}
}

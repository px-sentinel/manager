package net.poundex.sentinel.ui.manager.ui.sensor

import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class EditValueReaderWindow extends ResourceFormWindow<EditValueReaderForm>
{
	final String title = "Edit Value Reader"

	EditValueReaderWindow(ResourceClient resourceClient)
	{
		super(resourceClient, "valueReader")
	}

	static String role(String s)
	{
		return "EditValueReaderWindow/${s}"
	}
}

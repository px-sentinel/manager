package net.poundex.sentinel.ui.manager.ui

import net.poundex.sentinel.ui.manager.FormBean
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.WindowManager
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow
import org.springframework.beans.factory.ObjectFactory

trait ResourceSupport
{
	public <F extends FormBean, W extends ResourceFormWindow<F>> void addResource(WindowManager windowManager, Class<W> windowClass, ObjectFactory<W> objectFactory, F form, Runnable refresher)
	{
		windowManager.activateWindow(windowClass.role("new")) {
			W w = objectFactory.getObject()
			w.parentWindowNotifier = refresher
			w.form = form
			return w
		}
	}

	public <F extends FormBean, W extends ResourceFormWindow<F>> void editResource(Map current, WindowManager windowManager, Class<W> windowClass, ObjectFactory<W> objectFactory, F form, Runnable refresher)
	{
		if( ! current) return
		windowManager.activateWindow(windowClass.role("${current.id}")) {
			W w = objectFactory.object
			w.parentWindowNotifier = refresher
			form.id = current.id
			form.applyFromMap(current)
			w.form = form
			return w
		}
	}

	void deleteResource(Map current, String resourceName, ResourceClient resourceClient, Runnable refresher)
	{
		if( ! current) return
		resourceClient.delete(resourceName, current.id)
		refresher.run()
	}
}

package net.poundex.sentinel.ui.manager.ui.sensor

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import net.poundex.sentinel.ui.manager.FormBean

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

abstract class AbstractEditMonitorForm extends FormBean
{
	SimpleStringProperty name = new SimpleStringProperty()
	ObjectProperty<Map> location = new SimpleObjectProperty<>()

	@NotBlank
	String getName()
	{
		return name.value
	}

	void setName(String name)
	{
		this.name.setValue(name)
	}

	@NotNull
	Map getLocation()
	{
		return location.value
	}

	void setLocation(Map location)
	{
		this.location.setValue(location)
	}

	@Override
	Map<String, ?> toMap()
	{
		return [name: getName(), location: getLocation()]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		setName(map.name)
		setLocation(map.location)
	}
}

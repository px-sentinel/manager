package net.poundex.sentinel.ui.manager.ui.sensor

import groovy.transform.Canonical
import groovy.transform.PackageScope
import javafx.beans.property.ReadOnlyStringWrapper
import javafx.collections.ObservableList
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import org.springframework.beans.factory.ObjectFactory

class SensorsView extends TabPane
{
	private final SensorsWindow sensorsWindow

	private final ObservableList<Map> valueReaders
	private final ObservableList<MonitorRowWrapper> monitors

	private TableView<Map> valueReadersTable
	private TableView<MonitorRowWrapper> monitorsTable

	SensorsView(SensorsWindow sensorsWindow, ObservableList<Map> valueReaders, ObservableList<MonitorRowWrapper> monitors)
	{
		this.sensorsWindow = sensorsWindow
		this.valueReaders = valueReaders
		this.monitors = monitors
		init()
	}

	void init()
	{
		setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE)
		tabs << new Tab("Value Readers", createValueReadersView())
		tabs << new Tab("Monitors", createMonitorsView())
	}

	private BorderPane createValueReadersView()
	{
		BorderPane bp = new BorderPane()

		Button addValueReader = new Button("Add")
		addValueReader.setOnAction sensorsWindow.&onAddValueReaderClicked

		Button editValueReader = new Button("Edit")
		editValueReader.setOnAction sensorsWindow.&onEditValueReaderClicked

		Button deleteValueReader = new Button("Delete")
		deleteValueReader.setOnAction sensorsWindow.&onDeleteValueReaderClicked

		bp.setTop(new ToolBar(addValueReader, editValueReader, deleteValueReader))

		valueReadersTable = new TableView<>(valueReaders)
		TableColumn<Map, Map> colName = new TableColumn<>("Name")
		this.valueReadersTable.columns << colName

		colName.setCellValueFactory { TableColumn.CellDataFeatures<Map, Map> f ->
			new ReadOnlyStringWrapper(f.value.name)
		}

		bp.setCenter(this.valueReadersTable)

		return bp
	}

	Map getCurrentValueReader()
	{
		return valueReadersTable.focusModel.focusedItem
	}

	private BorderPane createMonitorsView()
	{
		BorderPane bp = new BorderPane()

		MenuItem addBooleanMonitorButton = new MenuItem("Boolean Monitor")
		addBooleanMonitorButton.setOnAction sensorsWindow.&onAddBooleanMonitorClicked

		MenuItem addQuantityMonitorButton = new MenuItem("Quantity Monitor")
		addQuantityMonitorButton.setOnAction sensorsWindow.&onAddQuantityMonitorClicked

		MenuButton addMonitorMenuButton = new MenuButton("Add", null, addBooleanMonitorButton, addQuantityMonitorButton)

		Button edit = new Button("Edit")
		edit.setOnAction sensorsWindow.&onEditMonitorClicked

		Button delete = new Button("Delete")
		delete.setOnAction sensorsWindow.&onDeleteMonitorClicked

		bp.setTop(new ToolBar(addMonitorMenuButton, edit, delete))

		monitorsTable = new TableView<>(monitors)
		TableColumn<MonitorRowWrapper, MonitorRowWrapper> colName = new TableColumn<>("Name")
		this.monitorsTable.columns << colName

		colName.setCellValueFactory { TableColumn.CellDataFeatures<MonitorRowWrapper, MonitorRowWrapper> f ->
			new ReadOnlyStringWrapper(f.value.value.name)
		}

		bp.setCenter(this.monitorsTable)

		return bp
	}

	MonitorRowWrapper getCurrentMonitor()
	{
		return monitorsTable.focusModel.focusedItem
	}

	@PackageScope
	@Canonical
	static class MonitorRowWrapper<F extends AbstractEditMonitorForm, W extends AbstractEditMonitorWindow<F>>
	{
		Map value
		Class<W> windowClass
		ObjectFactory<W> objectFactory
		F form
	}
}

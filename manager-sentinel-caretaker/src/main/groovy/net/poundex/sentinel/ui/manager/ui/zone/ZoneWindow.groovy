package net.poundex.sentinel.ui.manager.ui.zone

import groovy.transform.PackageScope
import javafx.event.ActionEvent
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class ZoneWindow implements Window
{
	final static String ROLE = "ZonesWindow"
	final String title = "Zones"
	final double preferredHeight = 500
	final double preferredWidth = 400

	final ZoneWindowView view

	private final ResourceClient resourceClient
	private final WindowManager windowManager
	private final ObjectFactory<EditZoneWindow> editZoneWindowObjectFactory

	ZoneWindow(ResourceClient resourceClient, WindowManager windowManager, ObjectFactory<EditZoneWindow> editZoneWindowObjectFactory)
	{
		this.resourceClient = resourceClient
		this.windowManager = windowManager
		this.editZoneWindowObjectFactory = editZoneWindowObjectFactory
		this.view = new ZoneWindowView(this)

		refresh()
	}

	private void refresh() {
		view.zones = resourceClient.findAll("zone")
	}

	@PackageScope
	void onAddClicked(ActionEvent ae)
	{
		windowManager.activateWindow(EditZoneWindow.role("new")) {
			EditZoneWindow w = editZoneWindowObjectFactory.getObject()
			w.parentWindowNotifier = this.&refresh
			EditZoneForm f = new EditZoneForm()
			w.form = f
			return w
		}
	}

	@PackageScope
	void onEditClicked(ActionEvent ae)
	{
		Map current = view.currentSelection
		if ( ! current) return

		windowManager.activateWindow(EditZoneWindow.role(current.id.toString())) {
			EditZoneWindow w = editZoneWindowObjectFactory.getObject()
			w.parentWindowNotifier = this.&refresh
			EditZoneForm f = new EditZoneForm()
			f.id = current.id
			f.applyFromMap(current)
			w.form = f
			return w
		}
	}

	@PackageScope
	void onDeleteClicked(ActionEvent ae)
	{
		Map current = view.currentSelection
		if( ! current) return
		resourceClient.delete("zone", current.id)
		refresh()
	}

}

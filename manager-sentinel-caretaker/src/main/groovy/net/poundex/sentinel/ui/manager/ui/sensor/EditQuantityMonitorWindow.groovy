package net.poundex.sentinel.ui.manager.ui.sensor

import net.poundex.sentinel.ui.manager.ResourceClient
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class EditQuantityMonitorWindow extends AbstractEditMonitorWindow<EditQuantityMonitorForm>
{
	EditQuantityMonitorWindow(ResourceClient resourceClient)
	{
		super(resourceClient, "monitor/boolean")
	}

	static String role(String s)
	{
		return "EditBooleanMonitorWindow/${s}"
	}

	@Override
	List<String> getFormFields()
	{
		return ["name", "location", "aggregator"]
	}
}

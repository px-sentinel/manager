package net.poundex.sentinel.ui.manager.ui.zone

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import net.poundex.sentinel.ui.manager.FormBean

import javax.validation.constraints.NotBlank

class EditZoneForm extends FormBean
{
	StringProperty name = new SimpleStringProperty()
	ObjectProperty<Map> parent = new SimpleObjectProperty<>()

	@NotBlank
	String getName() {
		return name.value
	}

	void setName(String name) {
		this.name.value = name
	}

	@Override
	Map<String, ?> toMap()
	{
		return [
		        name: name.value,
				parent: [id: parent.value?.id]
		]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		name.value = map.name
		parent.value = map.parent
	}
}

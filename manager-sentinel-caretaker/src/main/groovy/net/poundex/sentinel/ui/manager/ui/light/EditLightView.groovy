package net.poundex.sentinel.ui.manager.ui.light


import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormView

class EditLightView extends ResourceFormView<EditLightForm>
{
	EditLightView(EditLightWindow editLightWindow)
	{
		super(editLightWindow)
	}
}

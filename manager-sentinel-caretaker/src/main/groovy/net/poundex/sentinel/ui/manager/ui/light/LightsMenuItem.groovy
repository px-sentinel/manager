package net.poundex.sentinel.ui.manager.ui.light

import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Component
class LightsMenuItem implements MenuItem
{
	final String label = "Lights"

	private final WindowManager windowManager
	private final ObjectFactory<LightsWindow> lightsWindowObjectFactory

	LightsMenuItem(WindowManager windowManager, ObjectFactory<LightsWindow> lightsWindowObjectFactory)
	{
		this.windowManager = windowManager
		this.lightsWindowObjectFactory = lightsWindowObjectFactory
	}

	@Override
	void activate()
	{
		windowManager.activateWindow(LightsWindow.ROLE, lightsWindowObjectFactory.&getObject)
	}
}

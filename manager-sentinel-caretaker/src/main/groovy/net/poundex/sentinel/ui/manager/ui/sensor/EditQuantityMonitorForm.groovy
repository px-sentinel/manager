package net.poundex.sentinel.ui.manager.ui.sensor

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import org.springframework.util.StringUtils

import javax.validation.constraints.NotNull

class EditQuantityMonitorForm extends AbstractEditMonitorForm
{
	ObjectProperty<Aggregator> aggregator = new SimpleObjectProperty<>()

	@NotNull
	String getAggregator()
	{
		return aggregator.value?.toString()
	}

	void setAggregator(String aggregator)
	{
		this.aggregator.setValue(StringUtils.hasText(aggregator) ? Aggregator.valueOf(aggregator) : null)
	}


	@Override
	Map<String, ?> toMap()
	{
		return super.toMap() + [aggregator: getAggregator()]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		super.applyFromMap(map)
		setAggregator(map.aggregator)
	}

	enum Aggregator {
		AVERAGE,
		NEWEST_VALUE
	}
}

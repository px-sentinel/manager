package net.poundex.sentinel.ui.manager.ui.zone

import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Component
class ZoneMenuItem implements MenuItem
{
	final String label = "Zones"

	private final WindowManager windowManager
	private final ObjectFactory<ZoneWindow> zoneWindowObjectFactory

	ZoneMenuItem(WindowManager windowManager, ObjectFactory<ZoneWindow> zoneWindowObjectFactory)
	{
		this.windowManager = windowManager
		this.zoneWindowObjectFactory = zoneWindowObjectFactory
	}

	@Override
	void activate()
	{
		windowManager.activateWindow(ZoneWindow.ROLE, zoneWindowObjectFactory.&getObject)
	}
}

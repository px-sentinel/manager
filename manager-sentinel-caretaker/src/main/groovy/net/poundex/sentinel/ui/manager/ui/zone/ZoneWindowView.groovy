package net.poundex.sentinel.ui.manager.ui.zone

import javafx.beans.property.ReadOnlyStringWrapper
import javafx.scene.control.*
import javafx.scene.layout.BorderPane

class ZoneWindowView extends BorderPane
{
	private final ZoneWindow zoneWindow

	private final TreeItem<Map> rootNode = new TreeItem<>()
	private TreeTableView<Map> zonesView

	ZoneWindowView(ZoneWindow zoneWindow)
	{
		this.zoneWindow = zoneWindow
		setTop createToolbar()
		zonesView = createZonesView()
		setCenter zonesView
		setPrefSize(800, 800)
	}

	private TreeTableView createZonesView() {
		TreeTableColumn<Map, Map> colName = new TreeTableColumn<>("Zone")
		TreeTableView<Map> ttv = new TreeTableView<>(rootNode)
		ttv.columns << colName
		ttv.setShowRoot(false)

		colName.setCellValueFactory { TreeTableColumn.CellDataFeatures<Map, Map> f ->
			new ReadOnlyStringWrapper(f.value.value.name)
		}
		colName.setPrefWidth(400)

		return ttv
	}


	private ToolBar createToolbar()
	{
		Button addZone = new Button("Add")
		addZone.setOnAction(zoneWindow.&onAddClicked)

		Button editZone = new Button("Edit")
		editZone.setOnAction(zoneWindow.&onEditClicked)

		Button deleteZone = new Button("Delete")
		deleteZone.setOnAction(zoneWindow.&onDeleteClicked)

		return new ToolBar(addZone, editZone, deleteZone)
	}

	void setZones(List<Map<String, ?>> zones)
	{
		Map<Integer, TreeItem<Map>> treeItemsByZone = [:]
		zones.each {
			treeItemsByZone[it.id] = new TreeItem<Map>(it)
			treeItemsByZone.get(it.id).expanded = true
		}
		zones.each {
			Integer parentId = it.parent?.id
			if( ! parentId) return
			treeItemsByZone[parentId].children << treeItemsByZone[it.id]
		}
		rootNode.children.clear()
		rootNode.children.addAll(treeItemsByZone.values().groupBy { it.value.parent }[null])
	}

	Map getCurrentSelection()
	{
		return zonesView.getFocusModel().getFocusedItem().value
	}
}

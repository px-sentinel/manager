package net.poundex.sentinel.ui.manager.ui.sensor


import groovy.transform.PackageScope
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window
import net.poundex.sentinel.ui.manager.WindowManager
import net.poundex.sentinel.ui.manager.ui.ResourceSupport
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class SensorsWindow implements Window, ResourceSupport
{
	final static String ROLE = "SensorsWindow"

	final String title = "Sensors"
	final SensorsView view

	private final ResourceClient resourceClient
	private final WindowManager windowManager
	private final ObjectFactory<EditValueReaderWindow> editValueReaderWindowObjectFactory
	private final ObjectFactory<EditBooleanMonitorWindow> editBooleanMonitorWindowObjectFactory
	private final ObjectFactory<EditQuantityMonitorWindow> editQuantityMonitorWindowObjectFactory

	private final javafx.collections.ObservableList<Map> valueReaders = FXCollections.observableArrayList()
	private final javafx.collections.ObservableList<SensorsView.MonitorRowWrapper> monitors = FXCollections.observableArrayList()

	SensorsWindow(ResourceClient resourceClient, WindowManager windowManager, ObjectFactory<EditValueReaderWindow> editValueReaderWindowObjectFactory, ObjectFactory<EditBooleanMonitorWindow> editBooleanMonitorWindowObjectFactory, ObjectFactory<EditQuantityMonitorWindow> editQuantityMonitorWindowObjectFactory)
	{
		this.resourceClient = resourceClient
		this.view = new SensorsView(this, valueReaders, monitors)
		this.windowManager = windowManager
		this.editValueReaderWindowObjectFactory = editValueReaderWindowObjectFactory
		this.editBooleanMonitorWindowObjectFactory = editBooleanMonitorWindowObjectFactory
		this.editQuantityMonitorWindowObjectFactory = editQuantityMonitorWindowObjectFactory
		refresh()
	}

	void refresh()
	{
		valueReaders.clear()
		valueReaders.addAll(resourceClient.findAll("valueReader"))

		monitors.clear()
		monitors.addAll(
				resourceClient.findAll("monitor/boolean").collect {
					new SensorsView.MonitorRowWrapper(it, EditBooleanMonitorWindow,
							editBooleanMonitorWindowObjectFactory, new EditBooleanMonitorForm())
				} + resourceClient.findAll("monitor/quantity").collect {
					new SensorsView.MonitorRowWrapper(it, EditQuantityMonitorWindow,
							editQuantityMonitorWindowObjectFactory, new EditQuantityMonitorForm())
				})
	}

	@PackageScope
	void onAddValueReaderClicked(ActionEvent ae)
	{
		addResource(windowManager, EditValueReaderWindow, editValueReaderWindowObjectFactory,
				new EditValueReaderForm(), this.&refresh)
	}

	@PackageScope
	void onEditValueReaderClicked(ActionEvent ae)
	{
		editResource(view.currentValueReader, windowManager, EditValueReaderWindow, editValueReaderWindowObjectFactory,
				new EditValueReaderForm(), this.&refresh)
	}

	@PackageScope
	void onDeleteValueReaderClicked(ActionEvent ae)
	{
		deleteResource(view.currentValueReader, "valueReader", resourceClient, this.&refresh)
	}

	@PackageScope
	void onAddBooleanMonitorClicked(ActionEvent ae)
	{
		addResource(windowManager, EditBooleanMonitorWindow, editBooleanMonitorWindowObjectFactory,
				new EditBooleanMonitorForm(), this.&refresh)
	}

	@PackageScope
	void onAddQuantityMonitorClicked(ActionEvent ae)
	{
		addResource(windowManager, EditQuantityMonitorWindow, editQuantityMonitorWindowObjectFactory,
				new EditQuantityMonitorForm(), this.&refresh)
	}

	@PackageScope
	void onEditMonitorClicked(ActionEvent ae)
	{
		SensorsView.MonitorRowWrapper current = view.currentMonitor
		editResource(current.value, windowManager, current.windowClass, current.objectFactory,
			current.form, this.&refresh)
	}


}

package net.poundex.sentinel.ui.manager.ui.light

import groovy.transform.PackageScope
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class LightsWindow implements Window
{
	static final String ROLE = "LightsWindow"

	final String title = "Lights"
	final LightsView view

	private final ResourceClient resourceClient
	private final WindowManager windowManager
	private final ObjectFactory<EditLightWindow> editLightWindowObjectFactory

	private final javafx.collections.ObservableList<Map> lights = FXCollections.observableArrayList()

	LightsWindow(ResourceClient resourceClient, WindowManager windowManager, ObjectFactory<EditLightWindow> editLightWindowObjectFactory)
	{
		this.resourceClient = resourceClient
		this.windowManager = windowManager
		this.editLightWindowObjectFactory = editLightWindowObjectFactory

		view = new LightsView(this, lights)
		refresh()
	}

	void refresh()
	{
		lights.clear()
		lights.addAll(resourceClient.findAll("light"))
	}

	@PackageScope
	void onAddClicked(ActionEvent ae)
	{
		windowManager.activateWindow(EditLightWindow.role("new")) {
			EditLightWindow w = editLightWindowObjectFactory.getObject()
			w.parentWindowNotifier = this.&refresh
			EditLightForm f = new EditLightForm()
			w.form = f
			return w
		}
	}

	@PackageScope
	void onEditClicked(ActionEvent ae)
	{
		Map current = view.current
		if( ! current) return
		windowManager.activateWindow(EditLightWindow.role("${current.id}")) {
			EditLightWindow w = editLightWindowObjectFactory.object
			w.parentWindowNotifier = this.&refresh
			EditLightForm form = new EditLightForm()
			form.id = current.id
			form.applyFromMap(current)
			w.form = form
			return w
		}

	}

	@PackageScope
	void onDeleteClicked(ActionEvent ae)
	{
		Map current = view.current
		if( ! current) return
		resourceClient.delete("light", current.id)
		refresh()
	}
}

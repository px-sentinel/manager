package net.poundex.sentinel.ui.manager.ui.light

import com.dooapp.fxform.view.factory.DefaultFactoryProvider
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.ui.EntityChooser
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Lazy
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
class EditLightWindow extends ResourceFormWindow<EditLightForm> implements EntityChooser
{
	final String title = "Edit Light"

	EditLightWindow(ResourceClient resourceClient)
	{
		super(resourceClient, "light", new EditLightView(this))
	}

	static String role(String s)
	{
		return "EditLightWindow/" + s
	}

	@Override
	protected void customiseFactoryProvider(DefaultFactoryProvider defaultFactoryProvider)
	{
		entityChooser(defaultFactoryProvider, "location", "zone")
	}
}

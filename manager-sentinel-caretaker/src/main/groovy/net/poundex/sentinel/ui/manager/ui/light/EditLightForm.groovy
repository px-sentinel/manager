package net.poundex.sentinel.ui.manager.ui.light

import com.dooapp.fxform.annotation.FormFactory
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import net.poundex.sentinel.ui.manager.FormBean
import net.poundex.sentinel.ui.manager.ui.DeviceIdFormFieldFactory

import javax.validation.constraints.NotBlank

class EditLightForm extends FormBean
{
	StringProperty name = new SimpleStringProperty()
	@FormFactory(DeviceIdFormFieldFactory)
	StringProperty deviceId = new SimpleStringProperty()
	ObjectProperty<Map> location = new SimpleObjectProperty<>()

	@NotBlank
	String getName() {
		return name.value
	}

	void setName(String name)
	{
		this.name.setValue(name)
	}

	@NotBlank
	String getDeviceId() {
		return deviceId.value
	}

	void setDeviceId(String deviceId)
	{
		this.deviceId.setValue(deviceId)
	}

	@Override
	Map<String, ?> toMap()
	{
		return [name: getName(), deviceId: getDeviceId(), location: [id: location.value?.id] ]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		setName(map.name)
		setDeviceId(map.deviceId)
		location.value = map.location
	}
}

package net.poundex.sentinel.ui.manager.ui

import com.dooapp.fxform.view.FXFormNode
import com.dooapp.fxform.view.FXFormNodeWrapper
import javafx.scene.control.TextField
import javafx.scene.input.DragEvent
import javafx.scene.input.TransferMode
import javafx.util.Callback

class DeviceIdFormFieldFactory implements Callback<Void, FXFormNode>
{
	@Override
	FXFormNode call(Void param)
	{
		TextField textField = new TextField()

		textField.setOnDragOver({ DragEvent event ->
			event.acceptTransferModes(TransferMode.COPY)
			event.consume()
		})

		textField.setOnDragDropped({ DragEvent event ->
			textField.setText(event.dragboard.string)
			event.setDropCompleted(true)
			event.consume()
		})

		return new FXFormNodeWrapper(textField, textField.textProperty())
	}
}

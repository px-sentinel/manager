package net.poundex.sentinel.ui.manager.ui.resourceform

import com.dooapp.fxform.FXForm
import javafx.collections.ListChangeListener
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import net.poundex.sentinel.ui.manager.FormBean

import javax.validation.ConstraintViolation

class ResourceFormView<T extends FormBean> extends BorderPane
{
	private final ResourceFormWindow<T> resourceFormWindow

	protected Button btnOk = new Button("OK")
	protected Button btnApply = new Button("Apply")
	protected Button btnCancel = new Button("Cancel")

	ResourceFormView(ResourceFormWindow<T> resourceFormWindow)
	{
		this.resourceFormWindow = resourceFormWindow

		init()
	}

	private void init()
	{
		setRight createButtons()
		btnOk.setOnAction(resourceFormWindow.&onOk)
		btnApply.setOnAction(resourceFormWindow.&onApply)
		btnCancel.setOnAction(resourceFormWindow.&onCancel)
	}


	private VBox createButtons() {
		VBox vBox = new VBox()
		vBox.children << btnOk
		vBox.children << btnApply
		vBox.children << btnCancel
		additionalButtons.each { buttons ->
			vBox.children << new Label("")
			buttons.each { button ->
				vBox.children << button
			}
		}
		return vBox
	}

	protected List<List<Button>> getAdditionalButtons() {
		return []
	}

	void setForm(FXForm form)
	{
		form.constraintViolations.addListener({ ListChangeListener.Change<? extends ConstraintViolation> c ->
			[btnOk, btnApply].each {
				it.setDisable( ! c.list.empty)
			}
		} as ListChangeListener)


		setCenter form
	}
}

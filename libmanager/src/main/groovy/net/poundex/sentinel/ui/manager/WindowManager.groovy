package net.poundex.sentinel.ui.manager


import java.util.function.Supplier

interface WindowManager
{
	public <T extends Window> void activateWindow(String role, Supplier<T> windowSupplier)
}

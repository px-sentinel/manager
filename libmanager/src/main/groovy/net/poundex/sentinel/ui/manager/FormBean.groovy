package net.poundex.sentinel.ui.manager

import com.dooapp.fxform.annotation.NonVisual

abstract class FormBean
{
	@NonVisual
	Long id

	abstract Map<String, ?> toMap()
	abstract void applyFromMap(Map<String, ?> map)
}

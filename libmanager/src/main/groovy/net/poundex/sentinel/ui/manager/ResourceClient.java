package net.poundex.sentinel.ui.manager;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface ResourceClient
{
	List<Map<String, ?>> findAll(String resourceName);

	Map save(String resourceName, Map<String, ?> payload);

	Map update(String resourceName, Serializable id, Map<String, ?> payload);

	void delete(String resourceName, Serializable id);
}

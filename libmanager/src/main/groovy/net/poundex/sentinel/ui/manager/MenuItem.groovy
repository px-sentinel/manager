package net.poundex.sentinel.ui.manager

interface MenuItem
{
	String getLabel()
	void activate()
}

package net.poundex.sentinel.ui.manager;

import javafx.scene.Node;

public interface Window
{
	String getTitle();

	default double getPreferredWidth() {
		return 500;
	}

	default double getPreferredHeight() {
		return 350;
	}

	default void setParentWindowNotifier(Runnable notifier) {

	}

	default void setWindowCloser(Runnable closer) {

	}

	Node getView();
}

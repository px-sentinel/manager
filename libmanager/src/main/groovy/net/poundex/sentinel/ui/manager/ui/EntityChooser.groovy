package net.poundex.sentinel.ui.manager.ui

import com.dooapp.fxform.AbstractFXForm
import com.dooapp.fxform.model.Element
import com.dooapp.fxform.view.factory.DefaultFactoryProvider
import com.dooapp.fxform.view.factory.impl.FXFormChoiceBoxNode
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import javafx.util.StringConverter
import net.poundex.sentinel.ui.manager.ResourceClient

trait EntityChooser
{
	void entityChooser(DefaultFactoryProvider defaultFactoryProvider, String elementName, String resourceName)
	{
		defaultFactoryProvider.addFactory({ e -> e.name == elementName}) {
			return new ChoiceNode(this.resourceClient, resourceName)
		}
	}

	private static class ChoiceNode extends FXFormChoiceBoxNode
	{
		private final ResourceClient resourceClient
		private final String resourceName

		ChoiceNode(ResourceClient resourceClient, String resourceName)
		{
			this.resourceClient = resourceClient
			this.resourceName = resourceName
		}

		@Override
		void init(Element element, AbstractFXForm fxForm)
		{
			choiceBox.itemsProperty().bind(new SimpleListProperty<>(
					FXCollections.observableArrayList(
							[null] + resourceClient.findAll(resourceName)
					)
			))
			choiceBox.getSelectionModel().select(element.getValue())
			choiceBox.setConverter(new StringConverter<Map>() {

				@Override
				String toString(Map object)
				{
					return object?.name ?: "<none>"
				}

				@Override
				Map fromString(String string)
				{
					return null
				}
			})
		}

		@Override
		void dispose()
		{
			choiceBox.itemsProperty().unbind()
			super.dispose()
		}

		@Override
		boolean isEditable()
		{
			return true
		}
	}
}

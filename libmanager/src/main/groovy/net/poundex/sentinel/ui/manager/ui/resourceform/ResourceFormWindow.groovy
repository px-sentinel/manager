package net.poundex.sentinel.ui.manager.ui.resourceform

import com.dooapp.fxform.FXForm
import com.dooapp.fxform.view.factory.DefaultFactoryProvider
import groovy.transform.PackageScope
import javafx.event.ActionEvent
import net.poundex.sentinel.ui.manager.FormBean
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window

abstract class ResourceFormWindow<T extends FormBean> implements Window
{
	final ResourceFormView<T> view

	protected final ResourceClient resourceClient
	protected final String resourceName

	protected T formBean
	protected Runnable notifier

	protected Runnable windowCloser

	ResourceFormWindow(ResourceClient resourceClient, String resourceName, ResourceFormView<T> view)
	{
		this.resourceClient = resourceClient
		this.resourceName = resourceName
		this.view = view
	}

	ResourceFormWindow(ResourceClient resourceClient, String resourceName)
	{
		this(resourceClient, resourceName, new ResourceFormView<T>(this))
	}

	void setForm(T formBean)
	{
		this.formBean = formBean

		FXForm form = new FXForm()
		view.form = form

		DefaultFactoryProvider factoryProvider = new DefaultFactoryProvider()
		customiseFactoryProvider(factoryProvider)
		doWithForm(form)
		form.setEditorFactoryProvider(factoryProvider)
		form.setSource(formBean)

	}

	protected void customiseFactoryProvider(DefaultFactoryProvider defaultFactoryProvider)
	{

	}

	protected void doWithForm(FXForm form)
	{

	}

	@PackageScope
	void onOk(ActionEvent ae) {
		onApply(ae)
		onCancel(ae)
	}

	@PackageScope
	void onApply(ActionEvent ae) {
		Map saved = formBean.id ?
			resourceClient.update(resourceName, formBean.id, formBean.toMap()) :
			resourceClient.save(resourceName, formBean.toMap())
		if(notifier)
			notifier.run()
		formBean.id = saved.id
	}

	@PackageScope
	void onCancel(ActionEvent ae) {
		windowCloser?.run()
	}

	@Override
	void setParentWindowNotifier(Runnable notifier)
	{
		this.notifier = notifier
	}

	@Override
	void setWindowCloser(Runnable closer)
	{
		this.windowCloser = closer
	}
}

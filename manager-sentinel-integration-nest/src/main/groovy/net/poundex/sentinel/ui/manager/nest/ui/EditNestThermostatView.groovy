package net.poundex.sentinel.ui.manager.nest.ui


import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormView

class EditNestThermostatView extends ResourceFormView<EditNestThermostatForm>
{
	EditNestThermostatView(EditNestThermostatWindow editNestThermostatWindow)
	{
		super(editNestThermostatWindow)
	}
}

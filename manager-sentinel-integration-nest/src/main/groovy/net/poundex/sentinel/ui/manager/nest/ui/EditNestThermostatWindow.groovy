package net.poundex.sentinel.ui.manager.nest.ui

import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.ui.resourceform.ResourceFormWindow
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Lazy
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
class EditNestThermostatWindow extends ResourceFormWindow<EditNestThermostatForm>
{
	final String title = "Edit Nest Thermostat"

	EditNestThermostatWindow(ResourceClient resourceClient)
	{
		super(resourceClient, "nest/thermostat", new EditNestThermostatView(this))
	}

	static String role(String s)
	{
		return "EditNestThermostatWindow/" + s
	}

}

package net.poundex.sentinel.ui.manager.nest.ui

import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Component
class NestMenuItem implements MenuItem
{
	final String label = "Nest"

	private final WindowManager windowManager
	private final ObjectFactory<NestWindow> nestWindowObjectFactory

	NestMenuItem(WindowManager windowManager, ObjectFactory<NestWindow> nestWindowObjectFactory)
	{
		this.windowManager = windowManager
		this.nestWindowObjectFactory = nestWindowObjectFactory
	}

	@Override
	void activate()
	{
		windowManager.activateWindow(NestWindow.ROLE, nestWindowObjectFactory.&getObject)
	}
}

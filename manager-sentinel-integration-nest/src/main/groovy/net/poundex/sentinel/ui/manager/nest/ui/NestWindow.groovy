package net.poundex.sentinel.ui.manager.nest.ui

import groovy.transform.PackageScope
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class NestWindow implements Window
{
	final static String ROLE = "NestWindow"
	final String title = "Nest"

	final NestWindowView view

	private final WindowManager windowManager
	private final ObjectFactory<EditNestThermostatWindow> editNestThermostatWindowObjectFactory
	private final ResourceClient resourceClient

	private final ObservableList<Map> thermostats = FXCollections.observableArrayList()

	NestWindow(WindowManager windowManager, ObjectFactory<EditNestThermostatWindow> editNestThermostatWindowObjectFactory, ResourceClient resourceClient) {
		this.windowManager = windowManager
		this.editNestThermostatWindowObjectFactory = editNestThermostatWindowObjectFactory
		this.resourceClient = resourceClient
		this.view = new NestWindowView(this, thermostats)
		refresh()
	}

	private void refresh() {
		thermostats.clear()
		thermostats.addAll(resourceClient.findAll("nest/thermostat"))
	}

	@PackageScope
	void onAddClicked(ActionEvent ae) {
		windowManager.activateWindow(EditNestThermostatWindow.role("new")) {
			EditNestThermostatWindow w = editNestThermostatWindowObjectFactory.getObject()
			w.parentWindowNotifier = this.&refresh
			EditNestThermostatForm f = new EditNestThermostatForm()
			f.name = "nest" + (thermostats.size() > 0 ? thermostats.size() : 0)
			w.form = f
			return w
		}
	}

	@PackageScope
	void onEditClicked(ActionEvent ae) {
		Map current = view.current
		if( ! current) return
		windowManager.activateWindow(EditNestThermostatWindow.role("${current.id}")) {
			EditNestThermostatWindow w = editNestThermostatWindowObjectFactory.object
			w.parentWindowNotifier = this.&refresh
			EditNestThermostatForm form = new EditNestThermostatForm()
			form.id = current.id
			form.applyFromMap(current)
			w.form = form
			return w
		}
	}

	@PackageScope
	void onDeleteClicked(ActionEvent ae) {
		Map current = view.current
		if( ! current) return
		resourceClient.delete("nest/thermostat", current.id)
		refresh()
	}
}

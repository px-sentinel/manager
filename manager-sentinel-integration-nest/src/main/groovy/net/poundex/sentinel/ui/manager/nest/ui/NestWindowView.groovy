package net.poundex.sentinel.ui.manager.nest.ui

import javafx.beans.property.ReadOnlyStringWrapper
import javafx.collections.ObservableList
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.BorderPane

class NestWindowView extends TabPane
{
	private final NestWindow nestWindow

	private final ObservableList<Map> thermostats

	private TableView<Map> thermostatsView

	NestWindowView(NestWindow nestWindow, ObservableList<Map> thermostats)
	{
		this.nestWindow = nestWindow
		this.thermostats = thermostats

		setTabClosingPolicy TabClosingPolicy.UNAVAILABLE
		tabs << new Tab("Thermostats", createThermostatView())
	}

	Node createThermostatView()
	{
		BorderPane bp = new BorderPane()
		Button add = new Button("Add")
		add.setOnAction nestWindow.&onAddClicked

		Button edit = new Button("Edit")
		edit.setOnAction nestWindow.&onEditClicked

		Button delete = new Button("Delete")
		delete.setOnAction nestWindow.&onDeleteClicked

		bp.setTop(new ToolBar(add, edit, delete))
		thermostatsView = createThermostatsView()
		bp.setCenter(thermostatsView)
		return bp
	}

	private TableView createThermostatsView() {
		TableColumn<Map, Map> colName = new TableColumn<>("Name")
//		TableColumn<Map, Map> colAddress = new TableColumn<>("Address")
		TableView<Map> table = new TableView<>(thermostats)
		table.columns.add colName//, colAddress
		colName.setCellValueFactory { f ->
			new ReadOnlyStringWrapper(f.value.name)
		}
//		colAddress.setCellValueFactory { f ->
//			new ReadOnlyStringWrapper(f.value.address)
//		}

		return table
	}

	Map getCurrent()
	{
		thermostatsView.focusModel.focusedItem
	}
}

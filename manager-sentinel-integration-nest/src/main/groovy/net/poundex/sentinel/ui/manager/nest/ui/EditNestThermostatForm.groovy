package net.poundex.sentinel.ui.manager.nest.ui

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import net.poundex.sentinel.ui.manager.FormBean

import javax.validation.constraints.NotBlank

class EditNestThermostatForm extends FormBean
{
	StringProperty name = new SimpleStringProperty()
	StringProperty nestDeviceId = new SimpleStringProperty()
	StringProperty nestAccessToken = new SimpleStringProperty()

	@NotBlank
	String getName() {
		return name.value
	}

	@NotBlank
	String getNestDeviceId() {
		return nestDeviceId.value
	}

	@NotBlank
	String getNestAccessToken() {
		return nestAccessToken.value
	}

	void setName(String name)
	{
		this.name.setValue(name)
	}

	void setNestDeviceId(String address)
	{
		this.nestDeviceId.value = address
	}

	void setNestAccessToken(String username)
	{
		this.nestAccessToken.value = username
	}

	@Override
	Map<String, ?> toMap()
	{
		return [name: getName(), nestDeviceId: getNestDeviceId(), nestAccessToken: getNestAccessToken()]
	}

	@Override
	void applyFromMap(Map<String, ?> map)
	{
		setName(map.name)
		setNestDeviceId(map.nestDeviceId)
		setNestAccessToken(map.nestAccessToken)
	}
}

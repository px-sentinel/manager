package net.poundex.sentinel.ui.manager.ui.main

import javafx.event.ActionEvent
import javafx.scene.control.Button
import javafx.scene.layout.VBox
import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Component
class MainMenu extends VBox
{
	private final List<MenuItem> menuItems
	private final WindowManager windowManager

	MainMenu(List<MenuItem> menuItems, WindowManager windowManager)
	{
		this.menuItems = menuItems
		this.windowManager = windowManager
	}

	@PostConstruct
	void init() {
		menuItems.each { item ->
			children.add( new Button(item.label).tap { btn ->
				btn.setOnAction { ActionEvent ae ->
					item.activate()
				}
			} )
		}
	}

}

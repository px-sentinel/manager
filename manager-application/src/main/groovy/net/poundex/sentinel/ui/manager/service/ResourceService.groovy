package net.poundex.sentinel.ui.manager.service

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import net.poundex.sentinel.ui.manager.ResourceClient
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import org.springframework.stereotype.Service

@Service
class ResourceService implements ResourceClient
{
	private final static String BASE_URL = "http://localhost:8080"
	private final OkHttpClient client = new OkHttpClient()

	@Override
	List<Map<String, ?>> findAll(String resourceName) {
		Request request = new Request.Builder()
				.url("${BASE_URL}/${resourceName}")
				.build()

		Response response
		try {
			response = client.newCall(request).execute()
			return new JsonSlurper().parse(response.body().charStream())
		} finally {
			response.close()
		}
	}

	@Override
	Map save(String resourceName, Map<String, ?> payload)
	{
		Request request = new Request.Builder()
				.url("${BASE_URL}/${resourceName}")
				.post(RequestBody.create(
					MediaType.parse("application/json"), JsonOutput.toJson(payload)))
				.build()

		Response response
		try {
			response = client.newCall(request).execute()
			return new JsonSlurper().parse(response.body().charStream())
		} finally {
			response.close()
		}
	}

	@Override
	Map update(String resourceName, Serializable id, Map<String, ?> payload)
	{
		Request request = new Request.Builder()
				.url("${BASE_URL}/${resourceName}/${id}")
				.put(RequestBody.create(
					MediaType.parse("application/json"), JsonOutput.toJson(payload)))
				.build()

		Response response
		try {
			response = client.newCall(request).execute()
			return new JsonSlurper().parse(response.body().charStream())
		} finally {
			response.close()
		}
	}

	@Override
	void delete(String resourceName, Serializable id)
	{
		Request request = new Request.Builder()
				.url("${BASE_URL}/${resourceName}/${id}")
				.delete()
				.build()

		Response response
		try {
			response = client.newCall(request).execute()
		} finally {
			response.close()
		}
	}
}

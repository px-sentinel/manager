package net.poundex.sentinel.ui.manager

import groovy.transform.CompileStatic
import javafx.application.Application
import javafx.stage.Stage
import net.poundex.sentinel.ui.manager.ctx.AppConfig
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.AnnotationConfigApplicationContext

@CompileStatic
class SentinelManagerApp extends Application
{

	@Override
	void start(Stage primaryStage) throws Exception {
		AnnotationConfigApplicationContext ctx =
				new AnnotationConfigApplicationContext()
		ctx.beanFactory.registerSingleton("primaryStage", primaryStage)
		ctx.register(AppConfig)
		ctx.refresh()
	}
}

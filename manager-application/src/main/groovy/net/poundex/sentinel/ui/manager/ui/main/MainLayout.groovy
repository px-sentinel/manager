package net.poundex.sentinel.ui.manager.ui.main

import javafx.scene.Scene
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Component
class MainLayout
{
	private final Stage primaryStage
	private final Desktop desktop
	private final MainMenu mainMenu

	MainLayout(Stage primaryStage, Desktop desktop, MainMenu mainMenu)
	{
		this.primaryStage = primaryStage
		this.desktop = desktop
		this.mainMenu = mainMenu
	}

	@PostConstruct
	void init() {
		BorderPane mainPane = new BorderPane()
		mainPane.setPrefSize(1024, 768)
		mainPane.center = desktop
		mainPane.left = mainMenu

		primaryStage.scene = new Scene(mainPane)
		primaryStage.show()
	}
}

package net.poundex.sentinel.ui.manager.ui.devicemanager

import javafx.beans.property.ReadOnlyStringWrapper
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeTableColumn
import javafx.scene.control.TreeTableRow
import javafx.scene.control.TreeTableView
import javafx.scene.input.ClipboardContent
import javafx.scene.input.Dragboard
import javafx.scene.input.MouseEvent
import javafx.scene.input.TransferMode
import javafx.scene.layout.BorderPane

class DeviceManagerWindowView extends BorderPane
{
	private final DeviceManagerWindow deviceManagerWindow

	private TreeTableView<Map> deviceView
	private final TreeItem<Map> rootNode = new TreeItem<>()

	DeviceManagerWindowView(DeviceManagerWindow deviceManagerWindow)
	{
		this.deviceManagerWindow = deviceManagerWindow
		deviceView = createDeviceView()
		setCenter deviceView
	}


	private TreeTableView<Map> createDeviceView()
	{
		TreeTableColumn<Map, Map> colName = new TreeTableColumn<>("Device")
		TreeTableView<Map> ttv = new TreeTableView<>(rootNode)
		ttv.columns << colName
		ttv.setShowRoot(false)

		colName.setCellValueFactory { TreeTableColumn.CellDataFeatures<Map, Map> f ->
			new ReadOnlyStringWrapper(f.value.value.name)
		}
		colName.setPrefWidth(400)

		ttv.setRowFactory({ TreeTableView<Map> param ->
			TreeTableRow<Map> row = new TreeTableRow<>()
			row.setOnDragDetected { MouseEvent e ->
				Dragboard db = row.startDragAndDrop(TransferMode.COPY)
				ClipboardContent cc = new ClipboardContent()
				cc.putString(row.getItem().deviceId)
				db.content = cc
				e.consume()
			}
			return row
		})

		return ttv
	}

	void setDevices(List<Map<String, ?>> devices)
	{
		rootNode.children.clear()

		devices.groupBy { it.hardware }.each { k, v ->
			TreeItem<Map> hwNode = new TreeItem<>(k)
			v.each { d ->
				TreeItem<Map> deviceNode = new TreeItem<>(d)
				hwNode.children << deviceNode
			}
			rootNode.children << hwNode
		}
	}
}

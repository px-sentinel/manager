package net.poundex.sentinel.ui.manager.service

import groovy.transform.CompileStatic
import javafx.geometry.Point2D
import net.poundex.sentinel.ui.manager.Window
import net.poundex.sentinel.ui.manager.WindowManager
import net.poundex.sentinel.ui.manager.ui.main.Desktop
import org.kordamp.desktoppanefx.scene.layout.InternalWindow
import org.kordamp.ikonli.javafx.FontIcon
import org.springframework.stereotype.Service

import java.util.concurrent.ThreadLocalRandom
import java.util.function.Supplier

@Service
@CompileStatic
class WindowManagerService implements WindowManager
{
	private final Desktop desktop
	private final Set<String> openWindows = new HashSet<>()

	WindowManagerService(Desktop desktop)
	{
		this.desktop = desktop
	}

	@Override
	public <T extends Window> void activateWindow(String role, Supplier<T> windowSupplier)
	{
		desktop.taskBar.findTaskBarIcon(role).ifPresentOrElse({
			it.restoreWindow()
		}, {
			if(openWindows.contains(role)) return

			T w = windowSupplier.get()
			InternalWindow iw = new InternalWindow(role,
					new FontIcon("mdi-application:20"),
					w.title,
					w.view)

			iw.setOnCloseRequest {
				openWindows.remove role
			}
			desktop.addInternalWindow(iw, new Point2D(
					ThreadLocalRandom.current().nextDouble(0, 250),
					ThreadLocalRandom.current().nextDouble(0, 250)
			))
			iw.setPrefSize(((Window)w).preferredWidth, ((Window)w).preferredHeight)
			((Window)w).setWindowCloser(iw.&closeWindow)
			openWindows << role
		})
	}
}

package net.poundex.sentinel.ui.manager.ctx

import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication(scanBasePackages = "net.poundex.sentinel.ui.manager")
class AppConfig
{

}

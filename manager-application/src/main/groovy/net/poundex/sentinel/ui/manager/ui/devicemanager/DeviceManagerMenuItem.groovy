package net.poundex.sentinel.ui.manager.ui.devicemanager

import net.poundex.sentinel.ui.manager.MenuItem
import net.poundex.sentinel.ui.manager.WindowManager
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Component
class DeviceManagerMenuItem implements MenuItem
{
	final String label = "Device Manager"

	private final ObjectFactory<DeviceManagerWindow> deviceManagerWindowObjectFactory
	private final WindowManager windowManager

	DeviceManagerMenuItem(ObjectFactory<DeviceManagerWindow> deviceManagerWindowObjectFactory, WindowManager windowManager)
	{
		this.deviceManagerWindowObjectFactory = deviceManagerWindowObjectFactory
		this.windowManager = windowManager
	}

	@Override
	void activate()
	{
		windowManager.activateWindow(DeviceManagerWindow.ROLE, deviceManagerWindowObjectFactory.&getObject)
	}
}

package net.poundex.sentinel.ui.manager.ui.devicemanager


import net.poundex.sentinel.ui.manager.ResourceClient
import net.poundex.sentinel.ui.manager.Window
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
@Lazy
class DeviceManagerWindow implements  Window
{
	static final String ROLE = "DeviceManagerWindow"

	final String title = "Device Manager"

	final DeviceManagerWindowView view

	private final ResourceClient resourceClient


	DeviceManagerWindow(ResourceClient resourceClient)
	{
		this.resourceClient = resourceClient
		this.view = new DeviceManagerWindowView(this)
		refresh()
	}


	private void refresh() {
		view.devices = resourceClient.findAll("device")
	}
}
